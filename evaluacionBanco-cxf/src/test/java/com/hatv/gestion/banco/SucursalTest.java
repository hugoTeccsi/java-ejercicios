package com.hatv.gestion.banco;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hatv.gestion.banco.models.Sucursal;
import com.hatv.gestion.banco.services.ISucursalService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class SucursalTest {

	@Autowired
	private ISucursalService sucursalService;

	@Test
	@DisplayName("test listar sucursal")
	public void listarSucursalTest() {
		List<Sucursal> listSucursal = sucursalService.listar();
		Assertions.assertTrue(!listSucursal.isEmpty());
	}

	@Test
	@DisplayName("test guardar sucursal")
	public void guardarSucursalTest() {
		Sucursal objSucursal = Sucursal.builder().nombre("SUCURSAL TEST").direccion("direccion test")
				.fechaRegistro(new Date()).build();
		sucursalService.guardar(objSucursal);
		Optional<Sucursal> findSucursal = sucursalService.listar().stream()
				.filter(p -> p.getNombre().equals("SUCURSAL TEST")).findFirst();
		Assertions.assertTrue(findSucursal.isPresent());
	}
	
	@Test
	@DisplayName("test modificar sucursal")
	public void modificarBancoTest() {
		final Long idSucursalTest = 1L;
		Sucursal objSucursal = Sucursal.builder().idSucursal(idSucursalTest).nombre("SUCURSAL UPDATE")
				.direccion("direccion update").fechaRegistro(new Date()).build();
		sucursalService.modificar(idSucursalTest, objSucursal);
		Optional<Sucursal> findSucursal = sucursalService.listar().stream()
				.filter(p -> p.getNombre().equals("SUCURSAL UPDATE")).findFirst();
		Assertions.assertTrue(findSucursal.isPresent());
	}

	@Test
	@DisplayName("test eliminar sucursal")
	public void eliminarBancoTest() {
		final Long idSucursalTest = 1L;
		sucursalService.eliminar(idSucursalTest);
		Optional<Sucursal> findSucursal = sucursalService.listar().stream()
				.filter(p -> p.getIdSucursal().compareTo(idSucursalTest) == 0).findFirst();
		Assertions.assertTrue(!findSucursal.isPresent());
	}
}
