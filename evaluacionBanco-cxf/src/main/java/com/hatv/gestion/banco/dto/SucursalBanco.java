package com.hatv.gestion.banco.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SucursalBanco {

	private Long idBanco;
	private Long idSucursal;
	private String nombreSucursal;
	private String direccionSucursal;

	public SucursalBanco(Long idBanco, Long idSucursal, String nombreSucursal, String direccionSucursal) {
		this.idBanco = idBanco;
		this.idSucursal = idSucursal;
		this.nombreSucursal = nombreSucursal;
		this.direccionSucursal = direccionSucursal;
	}

}
