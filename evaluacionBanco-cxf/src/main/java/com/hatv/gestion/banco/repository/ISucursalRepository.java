package com.hatv.gestion.banco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hatv.gestion.banco.dto.SucursalOrdenPago;
import com.hatv.gestion.banco.models.Sucursal;

public interface ISucursalRepository extends JpaRepository<Sucursal, Long> {

	@Query("SELECT new com.hatv.gestion.banco.dto.SucursalOrdenPago(s.idSucursal, o.idOrdenPago,o.monto,o.moneda,o.estado,o.fechaPago) FROM Sucursal s JOIN s.ordenesPago o")
	public List<SucursalOrdenPago> getOrdenPorSucursal();
}
