package com.hatv.gestion.banco.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SucursalOrdenPago {

	private Long idSucursal;
	private Long idOrdenPago;
	private BigDecimal monto;
	private String moneda;
	private String estado;
	private Date fechaPago;

	public SucursalOrdenPago(Long idSucursal, Long idOrdenPago, BigDecimal monto, String moneda, String estado,
			Date fechaPago) {
		this.idSucursal = idSucursal;
		this.idOrdenPago = idOrdenPago;
		this.monto = monto;
		this.moneda = moneda;
		this.estado = estado;
		this.fechaPago = fechaPago;
	}

}
