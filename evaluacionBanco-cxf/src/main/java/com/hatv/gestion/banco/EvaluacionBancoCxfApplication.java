package com.hatv.gestion.banco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluacionBancoCxfApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluacionBancoCxfApplication.class, args);
	}
}
