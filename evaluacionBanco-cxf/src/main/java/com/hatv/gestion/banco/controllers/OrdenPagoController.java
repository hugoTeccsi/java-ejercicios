package com.hatv.gestion.banco.controllers;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.hatv.gestion.banco.models.OrdenPago;
import com.hatv.gestion.banco.services.IOrdenPagoService;

@Path("/ordenPago")
@Produces(MediaType.APPLICATION_JSON)
public class OrdenPagoController {

	@Autowired
	private IOrdenPagoService ordenPagoService;

	@GET
	@Path("/listar")
	public Response listar() {
		return Response.ok(ordenPagoService.listar()).build();
	}

	@POST
	@Path("/guardar")
	public Response guardar(OrdenPago reqOrdenPago) {
		return Response.status(Status.CREATED).entity(ordenPagoService.guardar(reqOrdenPago)).build();
	}

	@PUT
	@Path("/modificar/{idOrdenPago}")
	public Response guardar(@PathParam("idOrdenPago") Long idOrdenPago, OrdenPago reqOrdenPago) {
		return Response.status(Status.CREATED).entity(ordenPagoService.modificar(idOrdenPago, reqOrdenPago)).build();
	}

	@DELETE
	@Path("/eliminar/{idOrdenPago}")
	public Response eliminar(@PathParam("idOrdenPago") Long idOrdenPago) {
		ordenPagoService.eliminar(idOrdenPago);
		return Response.status(Status.NO_CONTENT).entity("").build();
	}
}
