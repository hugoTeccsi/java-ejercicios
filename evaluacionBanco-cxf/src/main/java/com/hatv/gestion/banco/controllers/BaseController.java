package com.hatv.gestion.banco.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;

public class BaseController {

	protected ObjectMapper objectMapper = null;
	
	public BaseController() {
		objectMapper = new ObjectMapper();
	}
}
