package com.hatv.gestion.banco.controllers;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.hatv.gestion.banco.models.Banco;
import com.hatv.gestion.banco.services.IBancoService;

@Path("/banco")
@Produces(MediaType.APPLICATION_JSON)
public class BancoController {

	@Autowired
	private IBancoService bancoService;

	@GET
	@Path("/listar")
	public Response listarBancos() {
		return Response.ok(bancoService.listar()).build();
	}
	
	@POST
	@Path("/guardar")
	
	public Response guardar(Banco reqBanco) {
		return Response.status(Status.CREATED).entity(bancoService.guardar(reqBanco)).build();
	}
	
	@PUT
	@Path("/modificar/{idBanco}")
	public Response guardar(@PathParam("idBanco") Long idBanco,Banco  reqBanco) {
		return Response.status(Status.CREATED).entity(bancoService.modificar(idBanco, reqBanco)).build();
	}
	
	@DELETE
	@Path("/eliminar/{idBanco}")
	public Response eliminar(@PathParam("idBanco") Long idBanco) {
		bancoService.eliminar(idBanco);
		return Response.status(Status.NO_CONTENT).entity("").build();
	}
}
