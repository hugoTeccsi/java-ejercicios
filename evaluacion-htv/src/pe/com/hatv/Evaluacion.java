package pe.com.hatv;

import java.util.Arrays;
import java.util.List;

public class Evaluacion {

	public static void main(String[] args) {
		
		final String INPUT_CADENA = "**Casa 52";
		ChangeString.build(INPUT_CADENA);
		
		List<Integer> lista =Arrays.asList(2,1,4,5);
		CompleteRange.build(lista).forEach(System.out::println);
		
		final String CADENA_MONTO="0.1";
		MoneyParts.build(CADENA_MONTO).forEach(System.out::println);
	}

}
