package pe.com.hatv;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MoneyParts {

	public static List<List<BigDecimal>> build(String valor) {
		System.out.println("********* Problema 03 **********");
		List<List<BigDecimal>> salida = new ArrayList<>();
		List<BigDecimal> denominacion = Arrays.asList(new BigDecimal(0.05).setScale(2, 1),
				new BigDecimal(0.1).setScale(1, 1), new BigDecimal(0.2).setScale(1, 1),
				new BigDecimal(0.5).setScale(1, 1), new BigDecimal(1).setScale(0), new BigDecimal(2).setScale(0),
				new BigDecimal(5).setScale(0), new BigDecimal(10).setScale(0), new BigDecimal(20).setScale(0),
				new BigDecimal(50).setScale(0), new BigDecimal(100).setScale(0), new BigDecimal(200).setScale(0));
		BigDecimal monto = new BigDecimal(valor);
		denominacion.forEach(obj -> {
			List<BigDecimal> temp = new ArrayList<>();
			if (monto.compareTo(obj) == 0 || monto.compareTo(obj) == 1) {
				Integer item = monto.divide(obj).intValue();
				if (item > 0) {
					for (int i = 0; i < item; i++) {
						temp.add(obj);
					}
				}
			}
			salida.add(temp);
		});
		return salida.stream().filter(obj -> !obj.isEmpty()).collect(Collectors.toList());
	}
}
