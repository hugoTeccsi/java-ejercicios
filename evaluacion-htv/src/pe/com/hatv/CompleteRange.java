package pe.com.hatv;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CompleteRange {

	public static List<Integer> build(List<Integer> listaInteger) {
		System.out.println("********* Problema 02 **********");
		Integer valorMaximo = listaInteger.stream().sorted(Comparator.reverseOrder()).findFirst().orElse(0);
		Stream<Integer> list = Stream.iterate(1, n -> n + 1).limit(valorMaximo);
		return list.collect(Collectors.toList());
	}
}
