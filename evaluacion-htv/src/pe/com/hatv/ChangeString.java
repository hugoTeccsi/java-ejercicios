package pe.com.hatv;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ChangeString {

	public static String build(String cadena) {
		System.out.println("********* Problema 01 **********");
		IntStream character = cadena.chars();
		List<Integer> temp = character.boxed().collect(Collectors.toList());
		temp = temp.stream().map(m -> setEvaluarCaracter(m)).collect(Collectors.toList());

		IntStream newCadena = temp.stream().mapToInt(Integer::intValue);
		String respuesta = newCadena.mapToObj(c -> (char) c)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
		return respuesta;
	}
	
	private static boolean evaluarCaracter(Integer c) {
		return (c >= 97 && c <= 122) || (c >= 65 && c <= 90);
	}
	
	private static Integer setEvaluarCaracter(Integer m) {
		Integer caracter = 0;
		if (evaluarCaracter(m)) {
			if (m.compareTo(122) == 0) {
				caracter = 97;
			} else if (m.compareTo(90) == 0) {
				caracter = 65;
			} else {
				caracter = m + 1;
			}
		} else if (m.compareTo(164) == 0) {
			caracter = 111;
		} else if (m.compareTo(165) == 0) {
			caracter = 79;
		} else {
			caracter = m;
		}
		return caracter;
	}
}
