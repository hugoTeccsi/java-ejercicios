package com.hatv.gestion.banco;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hatv.gestion.banco.models.OrdenPago;
import com.hatv.gestion.banco.services.IOrdenPagoService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class OrdenPagoTest {

	@Autowired
	private IOrdenPagoService ordenPagoService;

	@Test
	@DisplayName("test listar orden pagos")
	public void listarOrdenPagosTest() {
		List<OrdenPago> listOrdenPago = ordenPagoService.listar();
		Assertions.assertTrue(!listOrdenPago.isEmpty());
	}

	@Test
	@DisplayName("test guardar orden pagos")
	public void guardarOrdenPagosTest() {
		BigDecimal valorMontoTest = new BigDecimal(10000);
		OrdenPago objOrdenPago = OrdenPago.builder().monto(valorMontoTest).moneda("SOLES").estado("DECLINADO")
				.fechaPago(new Date()).build();
		ordenPagoService.guardar(objOrdenPago);
		Optional<OrdenPago> findOrdenPago = ordenPagoService.listar().stream()
				.filter(p -> p.getMonto().compareTo(valorMontoTest) == 0).findFirst();
		Assertions.assertTrue(findOrdenPago.isPresent());
	}
	
	@Test
	@DisplayName("test modificar orden pago")
	public void modificarBancoTest() {
		final Long ID_ORDEN_PAGO_TEST = 1L;
		final String MONEDA_TEST = "DOLARES";
		final BigDecimal MONTO_TEST = new BigDecimal(100000);
		OrdenPago objOrdenPago = OrdenPago.builder().moneda(MONEDA_TEST).monto(MONTO_TEST).build();
		ordenPagoService.modificar(ID_ORDEN_PAGO_TEST, objOrdenPago);
		Optional<OrdenPago> findOrdenPago = ordenPagoService.listar().stream()
				.filter(p -> p.getMoneda().equals(MONEDA_TEST) && p.getMonto().compareTo(MONTO_TEST) == 0).findFirst();
		Assertions.assertTrue(findOrdenPago.isPresent());
	}

	@Test
	@DisplayName("test eliminar orden pago")
	public void eliminarBancoTest() {
		final Long ID_ORDEN_PAGO_TEST = 1L;
		ordenPagoService.eliminar(ID_ORDEN_PAGO_TEST);
		Optional<OrdenPago> findOrdenPago = ordenPagoService.listar().stream()
				.filter(p -> p.getIdOrdenPago().compareTo(ID_ORDEN_PAGO_TEST) == 0 && p.getEstado().equals("ANULADA"))
				.findFirst();
		Assertions.assertTrue(findOrdenPago.isPresent());
	}
}
