package com.hatv.gestion.banco;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hatv.gestion.banco.models.Banco;
import com.hatv.gestion.banco.services.IBancoService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class BancoTest {

	@Autowired
	private IBancoService bancoService;

	@Test
	@DisplayName("test listar bancos")
	public void listarBancosTest() {
		List<Banco> listBanco = bancoService.listar();
		Assertions.assertTrue(!listBanco.isEmpty());
	}

	@Test
	@DisplayName("test guardar banco")
	public void guardarBancoTest() {
		Banco objBanco = Banco.builder().nombre("BANCO TEST").direccion("direccion test").fechaRegistro(new Date())
				.build();
		bancoService.guardar(objBanco);
		Optional<Banco> findBanco = bancoService.listar().stream().filter(p -> p.getNombre().equals("BANCO TEST"))
				.findFirst();
		Assertions.assertTrue(findBanco.isPresent());
	}
	
	@Test
	@DisplayName("test modificar banco")
	public void modificarBancoTest() {
		final Long idBancoTest = 1L;
		Banco objBanco = Banco.builder().idBanco(idBancoTest).nombre("BANCO UPDATE").direccion("direccion update")
				.fechaRegistro(new Date()).build();
		bancoService.modificar(idBancoTest, objBanco);
		Optional<Banco> findBanco = bancoService.listar().stream().filter(p -> p.getNombre().equals("BANCO UPDATE"))
				.findFirst();
		Assertions.assertTrue(findBanco.isPresent());
	}
	
	@Test
	@DisplayName("test eliminar banco")
	public void eliminarBancoTest() {
		final Long idBancoTest = 1L;
		bancoService.eliminar(idBancoTest);
		Optional<Banco> findBanco = bancoService.listar().stream().filter(p -> p.getIdBanco().compareTo(idBancoTest)==0)
				.findFirst();
		Assertions.assertTrue(!findBanco.isPresent());
	}

}
