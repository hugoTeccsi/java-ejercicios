package com.hatv.gestion.banco.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hatv.gestion.banco.dto.SucursalBanco;
import com.hatv.gestion.banco.models.Banco;
import com.hatv.gestion.banco.services.IBancoService;

@Controller
@RequestMapping("api/banco/")
public class BancoController {

	@Autowired
	private IBancoService bancoService;

	@GetMapping(value = "listar", produces = "application/json")
	public ResponseEntity<List<Banco>> listarBancos() {
		List<Banco> response = bancoService.listar();
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PostMapping(value = "guardar", produces = "application/json")
	public ResponseEntity<Banco> listarBancos(@RequestBody Banco banco) {
		Banco response = bancoService.guardar(banco);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@PutMapping(value = "modificar/{idBanco}", produces = "application/json")
	public ResponseEntity<Banco> modificar(@PathVariable Long idBanco, @RequestBody Banco banco) {
		Banco response = bancoService.modificar(idBanco, banco);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@DeleteMapping(value = "eliminar/{idBanco}", produces = "application/json")
	public ResponseEntity<String> eliminar(@PathVariable Long idBanco) {
		bancoService.eliminar(idBanco);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
	}

	@GetMapping(value = "listarSucursal/{idBanco}", produces = MediaType.APPLICATION_XML_VALUE, headers = "Accept=application/xml")
	public ResponseEntity<List<SucursalBanco>> listarSucursal(@PathVariable Long idBanco) {
		List<SucursalBanco> lista = bancoService.listarPorBanco(idBanco);
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}

}
