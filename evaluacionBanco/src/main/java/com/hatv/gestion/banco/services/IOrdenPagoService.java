package com.hatv.gestion.banco.services;

import java.util.List;

import com.hatv.gestion.banco.models.OrdenPago;

public interface IOrdenPagoService {

	public List<OrdenPago> listar();

	public OrdenPago guardar(OrdenPago ordenPago);

	public OrdenPago modificar(Long idOrdenPago, OrdenPago ordenPago);

	public void eliminar(Long idOrdenPago);

	public OrdenPago procesarOrden(Long idSucursal, OrdenPago ordenPago);
}
