package com.hatv.gestion.banco.services;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hatv.gestion.banco.dto.SucursalBanco;
import com.hatv.gestion.banco.models.Banco;
import com.hatv.gestion.banco.repository.IBancoRepository;

@Service
public class BancoService implements IBancoService {

	@Autowired
	private IBancoRepository bancoRepository;

	@Override
	public List<Banco> listar() {
		return bancoRepository.findAll();
	}

	@Override
	public Banco guardar(Banco banco) {
		return bancoRepository.save(banco);
	}

	@Override
	public Banco modificar(Long idBanco, Banco banco) {
		Banco obj = bancoRepository.findById(idBanco).get();
		obj.setNombre(banco.getNombre());
		obj.setDireccion(banco.getDireccion());
		obj.setFechaRegistro(new Date());
		return bancoRepository.save(obj);
	}

	@Override
	public void eliminar(Long idBanco) {
		bancoRepository.deleteById(idBanco);
	}

	@Override
	public List<SucursalBanco> listarPorBanco(Long idBanco) {
		return bancoRepository.getSucursalBanco().stream().filter(p -> p.getIdBanco().equals(idBanco))
				.collect(Collectors.toList());
	}

}
