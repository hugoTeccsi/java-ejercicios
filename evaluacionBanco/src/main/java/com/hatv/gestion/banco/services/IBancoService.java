package com.hatv.gestion.banco.services;

import java.util.List;

import com.hatv.gestion.banco.dto.SucursalBanco;
import com.hatv.gestion.banco.models.Banco;

public interface IBancoService {

	public List<Banco> listar();

	public Banco guardar(Banco banco);

	public Banco modificar(Long idBanco, Banco banco);

	public void eliminar(Long idBanco);

	public List<SucursalBanco> listarPorBanco(Long idBanco);

}
