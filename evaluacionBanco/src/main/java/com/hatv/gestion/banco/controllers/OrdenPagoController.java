package com.hatv.gestion.banco.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hatv.gestion.banco.models.OrdenPago;
import com.hatv.gestion.banco.services.IOrdenPagoService;

@RestController
@RequestMapping("api/ordenPago/")
public class OrdenPagoController {

	@Autowired
	private IOrdenPagoService ordenPagoService;

	@GetMapping("listar")
	public ResponseEntity<List<OrdenPago>> listar() {
		List<OrdenPago> response = ordenPagoService.listar();
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PostMapping("guardar")
	public ResponseEntity<OrdenPago> guardar(@RequestBody OrdenPago ordenPago) {
		OrdenPago response = ordenPagoService.guardar(ordenPago);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@PutMapping(value = "modificar/{idOrdenPago}")
	public ResponseEntity<OrdenPago> modificar(@PathVariable Long idOrdenPago, @RequestBody OrdenPago ordenPago) {
		OrdenPago response = ordenPagoService.modificar(idOrdenPago, ordenPago);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@PutMapping("eliminar/{idOrdenPago}")
	public ResponseEntity<String> eliminar(@PathVariable Long idOrdenPago) {
		ordenPagoService.eliminar(idOrdenPago);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
	}
}
