package com.hatv.gestion.banco.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hatv.gestion.banco.dto.SucursalOrdenPago;
import com.hatv.gestion.banco.models.OrdenPago;
import com.hatv.gestion.banco.models.Sucursal;
import com.hatv.gestion.banco.services.IOrdenPagoService;
import com.hatv.gestion.banco.services.ISucursalService;

@RestController
@RequestMapping("api/sucursal/")
public class SucursalController {

	@Autowired
	private ISucursalService sucursalService;

	@Autowired
	private IOrdenPagoService ordenPagoService;

	@GetMapping("listar")
	public ResponseEntity<List<Sucursal>> listar() {
		List<Sucursal> response = sucursalService.listar();
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PostMapping("guardar")
	public ResponseEntity<Sucursal> guardar(@RequestBody Sucursal Sucursal) {
		Sucursal response = sucursalService.guardar(Sucursal);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@PutMapping(value = "modificar/{idSucursal}")
	public ResponseEntity<Sucursal> modificar(@PathVariable Long idSucursal, @RequestBody Sucursal Sucursal) {
		Sucursal response = sucursalService.modificar(idSucursal, Sucursal);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@DeleteMapping("eliminar/{idSucursal}")
	public ResponseEntity<String> eliminar(@PathVariable Long idSucursal) {
		sucursalService.eliminar(idSucursal);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
	}

	@PostMapping("procesadorOrden/{idSucursal}")
	public ResponseEntity<OrdenPago> procesarOrden(@PathVariable Long idSucursal, @RequestBody OrdenPago ordenPago) {
		OrdenPago response = ordenPagoService.procesarOrden(idSucursal, ordenPago);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@GetMapping("listarPorSucursal/{idSucursal}")
	public ResponseEntity<List<SucursalOrdenPago>> listar(@PathVariable Long idSucursal, @RequestParam String moneda) {
		List<SucursalOrdenPago> response = sucursalService.listarPorSucursal(idSucursal, moneda);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
