package com.hatv.gestion.banco.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hatv.gestion.banco.dto.SucursalOrdenPago;
import com.hatv.gestion.banco.models.Sucursal;
import com.hatv.gestion.banco.repository.ISucursalRepository;

@Service
public class SucursalService implements ISucursalService {

	@Autowired
	private ISucursalRepository sucursalRepository;

	@Override
	public List<Sucursal> listar() {
		return sucursalRepository.findAll();
	}

	@Override
	public Sucursal guardar(Sucursal sucursal) {
		return sucursalRepository.save(sucursal);
	}

	@Override
	public Sucursal modificar(Long idSucursal, Sucursal sucursal) {
		Sucursal obj = sucursalRepository.findById(idSucursal).get();
		obj.setNombre(sucursal.getNombre());
		obj.setDireccion(sucursal.getDireccion());
		obj.setFechaRegistro(sucursal.getFechaRegistro());
		return sucursalRepository.save(obj);
	}

	@Override
	public void eliminar(Long idSucursal) {
		sucursalRepository.deleteById(idSucursal);
	}

	@Override
	public List<SucursalOrdenPago> listarPorSucursal(Long idSucursal, String moneda) {
		List<SucursalOrdenPago> respuesta = sucursalRepository.getOrdenPorSucursal();
		respuesta = respuesta.stream().filter(p -> p.getIdSucursal().equals(idSucursal) && p.getMoneda().equals(moneda))
				.collect(Collectors.toList());
		return respuesta;
	}

}
