package com.hatv.gestion.banco.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orden_pagos")
public class OrdenPago implements Serializable {

	private static final long serialVersionUID = -6537697063851833796L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long idOrdenPago;
	private BigDecimal monto;
	private String moneda;
	private String estado;
	@Column(name = "fecha_pago")
	@Temporal(TemporalType.DATE)
	private Date fechaPago;
}
