package com.hatv.gestion.banco.services;

import java.util.List;

import com.hatv.gestion.banco.dto.SucursalOrdenPago;
import com.hatv.gestion.banco.models.Sucursal;

public interface ISucursalService {

	public List<Sucursal> listar();

	public Sucursal guardar(Sucursal sucursal);

	public Sucursal modificar(Long idSucursal, Sucursal sucursal);

	public void eliminar(Long idSucursal);

	public List<SucursalOrdenPago> listarPorSucursal(Long idSucursal, String moneda);
}
