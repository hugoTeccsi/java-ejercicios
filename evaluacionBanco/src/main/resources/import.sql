insert into orden_pagos(id, monto,moneda,estado,fecha_pago) values (1, 100.0,'SOLES','ANULADA',CURRENT_DATE());
insert into orden_pagos(id, monto,moneda,estado,fecha_pago) values (2, 150.5,'SOLES','PAGADA',CURRENT_DATE());
insert into orden_pagos(id, monto,moneda,estado,fecha_pago) values (3, 300.5,'DOLARES','DECLINADA',CURRENT_DATE());

insert into bancos(nombre, direccion, fecha_registro) values('BANCO 1', 'AV. 987', CURRENT_DATE());
insert into bancos(nombre, direccion, fecha_registro) values('BANCO 2', 'AV. 567', CURRENT_DATE());

insert into sucursales(nombre, direccion, fecha_registro,banco_fk ) values('SUCURSAL 1','AV. 123',CURRENT_DATE(),1);
insert into sucursales(nombre, direccion, fecha_registro,banco_fk ) values('SUCURSAL 2','AV. 123',CURRENT_DATE(),1);
insert into sucursales(nombre, direccion, fecha_registro,banco_fk) values('SUCURSAL 3','AV. 543',CURRENT_DATE(),2);

